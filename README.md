# BA-Net: data-driven estimation of tissue attachments
<div align="center">
  <img width="800" height="160" src="pictures/teaser.png" alt="data-driven estimation of anatomical attachments">
</div>
This repository shares a dataset of manipulation (grasping and lifting) of deformable (silicone) phantoms with the dVRK Patient Side Manipulator (PSM) arm. The dataset consists of (1) starting undeformed phantom 3D models and (2) point clouds of the phantoms at increasing states of deformation.

Furthermore, this repository shares the pipeline used to predict the location of tissue attachment points using BA-Net. It allows to generate BA-Net input starting from the raw data from sensors, and to run inference. BA-Net architecture and pre-trained weights are provided.

If using the dataset or the pipeline, please cite the paper/s in the reference section below.

Overview:
* [Dataset description](#Dataset)
* [BA-Net pipeline](#Pipeline)
* [Setup](#Setup)
* [References](#References)

## Dataset
<div align="center">
  <img width="400" height="300" src="pictures/configurations.png" alt="dataset"/> 
</div>
The dataset can be downloaded at this [link](https://univr-my.sharepoint.com/:u:/g/personal/eleonora_tagliabue_univr_it/ESuS51EiqEFIpfcfrM-y8Z4BdvqUUnh5QtzGn2OoB1s39g?e=nOtzBa). Download and unzip dataset into the data/ directory. 

The acquired data are relative to manipulation (grasping and lifting) of 4 silicone phantoms with the dVRK Patient Side Manipulator (PSM) arm, characterized by

- different geometric shapes (circle, clover, rectangle and drop)
- different rest positions (flat or deformed)
- different configurations of attachment points 
- different grasping points

The configurations present in the collected dataset are schematically represented in the figure above. For each phantom, we choose 3 ***configurations of attachment points*** (red regions). For each attachments configuration, we select 3 or 4 ***grasping points*** (green dots). Moreover, for each configuration we consider two ***starting conditions***: the former where the phantom lies flat on the experimental base (flat) and the latter where the phantom starts from a pre-deformed state (deformed). For a thorough description of the setup and the experiments please refer to the paper ["Data-driven Intra-operative Estimation of Anatomical Attachments for Autonomous Tissue Dissection"](https://ieeexplore.ieee.org/document/9359348/). 

### Dataset structure
For each phantom, we have 6 folders: 3 are relative to flat starting condition (one for each configuration of attachments) and 3 are relative to deformed starting condition (one for each configuration of attachments). 

As an example, directory structure for circle experiment is detailed here:

data/  
+-- red_circle_1/	--> Experiment with a boundary configuration and an initial starting position  
&nbsp;&nbsp;&nbsp;&nbsp; +-- surface.stl 	--> starting surface mesh  
&nbsp;&nbsp;&nbsp;&nbsp; +-- volume_ground_truth.vtk  --> starting volume mesh which contains a vtkArray called "stiffness" where value = 1 is assigned to attachment points  
&nbsp;&nbsp;&nbsp;&nbsp; +-- 0/			--> Acquisitions relative to the first grasping point  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- psm.npy		--> EE coordinates relative to each of the acquired pcd (N points, one per acquisition)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- 00000/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- 00001/  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- ....  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- 0000N/        --> Acquisition relative to the Nth lifting level  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- partialDeformedSurface.ply	--> point cloud relative to the Nth lifting level (segmented and decimated)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- idxs.txt 						--> indices defining correspondences between pcd and surface.stl (*)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- voxelized.vts					--> 64x64x64 grid where information has been encoded  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- pred.vts						--> network prediction using BANet  
&nbsp;&nbsp;&nbsp;&nbsp; +-- 1/			--> Acquisitions relative to the second grasping point  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- psm.npy  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +-- ....  

(*) idxs.txt contains for each point of partialDeformedSurface.ply, the index of its corresponding point of surface.stl. 

#### Other files
- `properties.yml`: for each experiment, defines if the starting condition is flat or deformed (deformed: False/True) and if the attachment region is composed of two disjoint areas or not (disjoint: True/False).
- `area/` contains a .mat file for each acquired sample, following the naming convention in described above, containing the amount of visible surface (point cloud estimated surface wrt full surface).
- `rab_points.csv`: contains the 3D coordinates of the holes present in the RAB base.

#### Visualization
For visualization, we rely on [Paraview](https://www.paraview.org/) (tested with v 5.8.1). Open Paraview, go to File --> Load state, and select the .psvm state file present in visualization/ directory. From the dropdown menu, choose "Search files under specified directoy", and select the directory where you extracted the dataset.
In this way, you can visualize the acquired point cloud, the volume mesh with ground truth boundaries, the voxelized input, and the network prediction. You can toggle visibility of the different parts by clicking on the corresponding "eye" symbol in the panel on the left.
In order to visualize another sample, just select "Choose file names" when loading the state file, and select another sample. 

## Pipeline
<div align="center">
  <img width="700" height="180" src="pictures/pipeline.png" alt="pipeline">
</div>
This part describes how to predict the location of tissue attachments using BA-Net. An overview of the complete pipeline is provided by the figure above. For a thorough description of the pipeline please refer to our paper ["Intra-operative Update of Boundary Conditions for Patient-specific Surgical Simulation"]. 
The parameters provided as input to the different parts of the pipeline are specified in config/input_parameters.yml.

### Voxelization
By running `python voxelization.py`, it is possible to convert the raw input data into the format required by the network. We assume that the raw data have been already pre-processed (segmented, decimated, and rigidly aligned with the 3D surface model). The voxelization script allows to (1) calculate the Intra-Operative Displacement field (IOD), estimating corresponding points between the point cloud and the surface model using ZoomOut method, and (2) voxelize the input into a 64x64x64 grid.

### Inference
Inference can be run with `python inference.py`.

### Simulation
Soon...

## Setup
Please install the specified requirements.
Code in this repo has been tested with python 3.7, on a machine with Ubuntu 18.04. 

## References
Tagliabue, E., Dall’Alba, D., Pfeiffer, M. et al. ["Data-driven Intra-operative Estimation of Anatomical Attachments for Autonomous Tissue Dissection"](https://ieeexplore.ieee.org/document/9359348/) *IEEE Robotics and Automation Letters* (2021). 

Tagliabue, E., Piccinelli, M., Dall’Alba, D. et al. ["Intra-operative Update of Boundary Conditions for Patient-specific Surgical Simulation"](https://hal.archives-ouvertes.fr/hal-03315008/file/IntraopUpdateOfBCForSurgicalSimulation.pdf) *International Conference on Medical Image Computing and Computer Assisted Intervention - MICCAI* (2021). 

Altair Robotics Lab - University of Verona

*Contact*: eleonora[dot]tagliabue[at]univr[dot]it

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg