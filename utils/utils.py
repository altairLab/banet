import os
import yaml

#################################################################
# Config loading and handling
class ConfigMeta(type):
	def __getitem__( cls, key ):
		return cls.data[key]
	def __contains__( cls, key ):
		return key in cls.data

class Config(metaclass=ConfigMeta):
	data = []
	@staticmethod
	def load( filename ):
		with open(filename, 'r') as stream:
			Config.data = yaml.load(stream, Loader=yaml.SafeLoader)

def path(string):
    """
    Check if string is a valid path. If not, try to create it. If not, fail.
    """
    if os.path.isdir(string):
        return string
    else:
        try:
            os.makedirs(string)
            return string
        except:
            raise NotADirectoryError(string)

# Check if string is a valid file. If not, fail.
def filepath(string):
    """
    Check if string is a valid file. If not, fail.
    """
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)






