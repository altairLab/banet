import numpy as np
import math
from vtk import *
from vtk.util import numpy_support

#################################################################
# Grid related
def createGrid( size, gridSize ):
    grid = vtkStructuredGrid()
    grid.SetDimensions( (gridSize,gridSize,gridSize) )
    points = vtkPoints()
    points.SetNumberOfPoints( gridSize**3 )
    pID = 0
    start = -size/2
    d = size/(gridSize-1)
    for i in range( 0, gridSize ):
        for j in range( 0, gridSize ):
            for k in range( 0, gridSize ):
                x = start + d*k
                y = start + d*j
                z = start + d*i
                points.SetPoint( pID, x, y, z )
                pID += 1
    grid.SetPoints( points )
    return grid

def addDataToGrid( data, grid, field_name ):
    # Flatten data to 1D array
    data = np.ravel(data)
    data_vtk = numpy_support.numpy_to_vtk(data, array_type=VTK_FLOAT)
    data_vtk.SetName(field_name)
    if grid.GetPointData().GetArray(field_name):
        grid.GetPointData().RemoveArray(field_name)
    grid.GetPointData().AddArray( data_vtk )
    return grid

def loadMesh( filename ):
	"""
	Loads a mesh using VTK. Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	filename (str)
	
	Returns:
	--------
	vtkDataSet
		which is a vtkUnstructuredGrid or vtkPolyData, depending on the file type of the mesh.
	"""

	# Load the input mesh:
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		reader = vtkSTLReader()
		reader.SetFileName( filename )
		reader.Update()
		mesh = reader.GetOutput()
	elif fileType == ".obj":
		reader = vtkOBJReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".ply":
		reader = vtkPLYReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtk":
		reader = vtkUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtu":
		reader = vtkXMLUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtp":
		reader = vtkXMLPolyDataReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".pcd":
		import pcl
		pc = pcl.load( filename )
		pts = vtkPoints()
		verts = vtkCellArray()
		for i in range( pc.size ):
			pts.InsertNextPoint( pc[i][0], pc[i][1], pc[i][2] )
			verts.InsertNextCell( 1, (i,) )
		mesh = vtkPolyData()
		mesh.SetPoints( pts )
		mesh.SetVerts( verts )
 
	else:
		raise IOError("Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .ply or .pcd file!")

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Could not load a valid mesh from {}".format(filename))
	return mesh


def writeMesh( mesh, filename ):
	"""
	Saves a VTK mesh to file. 
	Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	mesh (vtkDataSet):
		mesh to save
	filename (str): 
		name of the file where to save the input mesh. MUST contain the desired extension.
	
	"""

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Input mesh has no points!")

	# Get file format
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		writer = vtkSTLWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update()
	elif fileType == ".obj":
		writer = vtkOBJWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".ply":
		writer = vtkPLYWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtk":
		writer = vtkUnstructuredGridWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtu":
		writer = vtkXMLUnstructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vts":
		writer = vtkXMLStructuredGridWriter() 
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	elif fileType == ".vtp":
		writer = vtkXMLPolyDataWriter()
		writer.SetFileName( filename )
		writer.SetInputData( mesh )
		writer.Update() 
	else:
		raise IOError("Supported extensions are .vtk, .vtu, .vts, .vtp, .obj, .stl, .ply!")

# Voxelize
def get_centering_transform( mesh ):
	bounds = [0]*6
	mesh.GetBounds(bounds)
	tf = vtkTransform()
	dx = -(bounds[1]+bounds[0])*0.5
	dy = -(bounds[3]+bounds[2])*0.5
	dz = -(bounds[5]+bounds[4])*0.5
	tf.Translate( (dx,dy,dz) )
	return tf

def apply_vtk_transform( mesh, transform ):
	tfFilter = vtkTransformFilter()
	tfFilter.SetTransform( transform )
	tfFilter.SetInputData( mesh )
	tfFilter.Update()
	return tfFilter.GetOutput()

def createGrid( size, gridSize ):
	grid = vtkStructuredGrid()
	grid.SetDimensions( (gridSize,gridSize,gridSize) )
	points = vtkPoints()
	points.SetNumberOfPoints( gridSize**3 )
	pID = 0
	start = -size/2
	d = size/(gridSize-1)
	for i in range( 0, gridSize ):
		for j in range( 0, gridSize ):
			for k in range( 0, gridSize ):
				x = start + d*k
				y = start + d*j
				z = start + d*i
				points.SetPoint( pID, x, y, z )
				pID += 1
	grid.SetPoints( points )
	return grid

def storeTransformationMatrix( grid, tf ):
	mat = tf.GetMatrix()
	matArray = vtkFloatArray()
	matArray.SetNumberOfTuples(16)
	matArray.SetNumberOfComponents(1)
	matArray.SetName( "TransformationMatrix" )
	for row in range(0,4):
		for col in range(0,4):
			matArray.SetTuple1( row*4+col, mat.GetElement( row, col ) )
	grid.GetFieldData().AddArray(matArray)

def distanceField( surfaceMesh, targetGrid, targetArrayName, signed=False ):
	# Initialize distance field:
	df = vtkFloatArray()
	df.SetNumberOfTuples( targetGrid.GetNumberOfPoints() )
	df.SetName(targetArrayName)

	# Data structure to quickly find cells:
	cellLocator = vtkCellLocator()
	cellLocator.SetDataSet( surfaceMesh )
	cellLocator.BuildLocator()


	for i in range(0, targetGrid.GetNumberOfPoints() ):
		# Take a point from the target...
		testPoint = [0]*3
		targetGrid.GetPoint( i, testPoint )
		# ... find the point in the surface closest to it
		cID, subID, dist2 = mutable(0), mutable(0), mutable(0.0)
		closestPoint = [0]*3
		
		cellLocator.FindClosestPoint( testPoint, closestPoint, cID, subID, dist2 )
		
		dist = math.sqrt(dist2)
		
		df.SetTuple1( i, dist )

	if signed:
		pts = vtkPolyData()
		pts.SetPoints( targetGrid.GetPoints() )

		enclosedPointSelector = vtkSelectEnclosedPoints()
		enclosedPointSelector.CheckSurfaceOn()
		enclosedPointSelector.SetInputData( pts )
		enclosedPointSelector.SetSurfaceData( surfaceMesh )
		enclosedPointSelector.SetTolerance( 1e-9 )
		enclosedPointSelector.Update()
		enclosedPoints = enclosedPointSelector.GetOutput()

		for i in range(0, targetGrid.GetNumberOfPoints() ):
			if enclosedPointSelector.IsInside(i):
				df.SetTuple1( i, -df.GetTuple1(i) )     # invert sign

	targetGrid.GetPointData().AddArray( df )

def addVertsToPolyData( pd ):
	verts = vtkCellArray()
	for i in range(pd.GetNumberOfPoints()):
		verts.InsertNextCell( 1, (i,) )
	pd.SetVerts(verts)

# VOXELIZE_DISPLACEMENT
def compute_warp_displacement( mesh, pcd, mesh_indices, min_visible_displacement=5e-3, invisible=False ):
	'''
	Given a mesh and a point cloud, associates each mesh point with the 
	corresponding point in the pcd based on the correspondences identified by mesh_indices
	and computes the displacement vector. The mesh is assumed to be originally flat, with
	flat surface normal to y direction. This association is important for displacement
	calculation of invisible points (which is based only on x and z coordinates, and not on y).

	Parameters
	----------
	mesh (vtkPolyData):
		Starting mesh whose vertices have to be associated with a displacement vector.
	pcd (vtkPolyData):
		Deformed view of the mesh, as a point cloud. 
	mesh_indices (list of ints):
		For each pcd point, contains the corresponding index of the mesh. 
	min_visible_displacement (float):
		If the mesh point is associated to a pcd point, but the magnitude of the computed displacement is
		lower than min_visible_displacement, the corresponding displacement vector is clipped to (1e-05, 1e-05, 1e-05).
		Default: 5e-3
	invisible (bool):
		If True, the displacement vector is computed for all the mesh points (both visible and invisible). For each of the 
		invisible points, the associated displacement is that of the closest visible point (neglecting y coordinate). 
		Default: False

	Returns
	----------
	vtkFloatArray:
		Nx3 displacement vector of each mesh point. 
	'''

	print("Calculating displacement of visible points" )
	displacement = vtkFloatArray()
	displacement.SetName( "displacement" )
	displacement.SetNumberOfComponents( 3 )
	displacement.SetNumberOfTuples( mesh.GetNumberOfPoints() )
	displacement.Fill(0.0)

	#################################################
	## Calculate input displacement at visible points
	# Keep only closest correspondences
	distance_vector = np.array([np.nan]* mesh.GetNumberOfPoints())
	vol_points = vtkPoints()

	for idx_intraop, idx_vol in enumerate( mesh_indices ):
		p1 = mesh.GetPoint( idx_vol )
		p2 = pcd.GetPoint( idx_intraop )
		displ = (p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2])

		distance = np.linalg.norm( np.asarray( displ ) )
		# Replace the displacement...
		# ...if it has not been computed yet
		if distance > min_visible_displacement:
			if np.isnan(distance_vector[idx_vol]):
				distance_vector[idx_vol] = distance
				displacement.SetTuple3( idx_vol, displ[0], displ[1], displ[2] )
			# ...or if distance is lower than the previous
			elif distance < distance_vector[idx_vol]:
				distance_vector[idx_vol] = distance
				displacement.SetTuple3( idx_vol, displ[0], displ[1], displ[2] )
		else:
			# The displacement is almost null, but the area is visible.
			distance_vector[idx_vol] = distance
			displacement.SetTuple3( idx_vol, 1e-05, 1e-05, 1e-05 )

		# Copy point coordinates vol_points_list, neglecting y
		vol_points.InsertNextPoint( p1[0], 0.0, p1[2] )
	
	#################################################
	## Associate displacement to invisible points
	if invisible:
		vol_points_poly = vtkPolyData()
		vol_points_poly.SetPoints( vol_points )

		locator = vtkPointLocator( )
		locator.SetDataSet( vol_points_poly )
		locator.SetNumberOfPointsPerBucket(1)
		locator.BuildLocator()

		# For those idx with associated nan distance, get closest point (in x and z) with not nan distance
		# and associate the same displacement
		for j in range( mesh.GetNumberOfPoints() ):
			if np.isnan( distance_vector[j] ):
				point = mesh.GetPoint(j)
				y_point = ( point[0], 0.0, point[2] )

				ind_min_y_diff = locator.FindClosestPoint( y_point )

				#ind_min_y_diff = np.argmin( np.abs( y_vol_points_list - y ) )
				closestYInd = mesh_indices[ ind_min_y_diff ]
				p_displ = displacement.GetTuple3( closestYInd )
				displacement.SetTuple3( j, p_displ[0], p_displ[1], p_displ[2] )
				distance_vector[j] = np.copy( distance_vector[closestYInd] )

	return displacement
    
def interpolateToGrid( mesh, grid, cellSize, sharpness=10, radius=None ):
	"""     
	Interpolates DataArrays present in mesh onto the grid using a Gaussian Kernel. 
	Be careful because all DataArrays belonging to mesh will be interpolated.
	Default kernel radius is defined as 5*cellSize.

	Arguments:
	---------
	mesh (vtkDataSet):
		Initial mesh with the fields to be interpolated into grid.
	grid (vtkDataSet):
		Grid where the DataArrays will be interpolated to.
	cellSize (list of ints):
		Size of the grid cells.
	sharpness (int):
		Sharpness of the Gaussian kernel. As the sharpness increases, the effect of distant points decreases.
		Default: 10
	radius (float):
		Radius of the Gaussian kernel. If not specified, kernel radius is defined as 5*cellSize.
		Default: None

	Returns:
	--------
	vtkDataSet
		Same as mesh_initial where a new DataArray "displacement" has been added
	"""
	if not radius:
		radius = cellSize * 5

	# Perform the interpolation
	interpolator = vtkPointInterpolator()
	gaussianKernel = vtkGaussianKernel()
	gaussianKernel.SetRadius( radius )
	gaussianKernel.SetSharpness(sharpness)
	interpolator.SetSourceData( mesh )
	interpolator.SetInputData( grid )
	interpolator.SetKernel( gaussianKernel )
	#interpolator.SetNullPointsStrategy(vtkPointInterpolator.CLOSEST_POINT)
	interpolator.Update()

	#output = interpolator.GetOutput()
	output = vtkStructuredGrid()
	output.DeepCopy(interpolator.GetOutput())
	del interpolator, gaussianKernel
	return output

def spreadValuesOnGridPoints( mesh, grid, field_name, radius=None, binary=False ):
	""" 
	Transfers the mesh field (Tuple1) described by field_name to the grid.
	For each mesh point, finds the grid cell it falls into and associated the mesh field value
	to all the grid nodes defining the corresponding cell. If radius is specified, it associates the 
	the mesh field value to all the grid nodes whose distance from the corresponding mesh point 
	is lower than that radius. 
	Note that no interpolation is computed here (values are just transferred to grid as they are).

	Arguments:
	---------
	mesh (vtkDataSet):
		Topology of the mesh
	grid (vtkDataSet):
		Topology of the grid
	field_name (str):
		Name of the DataArray present in mesh that has to be transferred to the grid.
	radius (float):
		Maximum distance between a cell point and a mesh point to have the mesh field associated.
		Default: None 
	binary (bool):
		If True, when field is different from 0, sets the grid value to 1.
		Default: False

	Returns:
	--------
	vtkDataSet
		Same as grid where the new DataArray "field_name" has been added
	"""

	# Create array for grid
	grid_array = vtkDoubleArray()
	grid_array.SetNumberOfComponents(1)
	grid_array.SetName(field_name)
	grid_array.SetNumberOfTuples(grid.GetNumberOfPoints())
	grid_array.Fill(0.0)

	if radius:
		locator = vtkPointLocator()
	else:
		locator = vtkCellLocator()
	locator.SetDataSet(grid)
	locator.Update()
	cell_pts_idx = vtkIdList()

	# Get data array in mesh
	mesh_array = mesh.GetPointData().GetArray(field_name)

	for i in range(mesh_array.GetNumberOfTuples()):
		field_value = mesh_array.GetTuple1(i)
		if not field_value == 0.0:

			if radius:
				locator.FindPointsWithinRadius( radius, mesh.GetPoint(i), cell_pts_idx)
			else:
				cell = locator.FindCell(mesh.GetPoint(i))
				grid.GetCellPoints(cell,cell_pts_idx)

			if binary:
				field_value = 1.0
			for j in range(cell_pts_idx.GetNumberOfIds()):
				grid_array.SetTuple1(cell_pts_idx.GetId(j), field_value)

	grid.GetPointData().AddArray( grid_array )   
	return grid