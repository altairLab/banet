import torch
import torch.nn as nn

class BANet(nn.Module):

    def __init__(self, in_channels=4, out_channels=1, init_features=8):
        super(BANet, self).__init__()

        # Common
        self.activation = nn.ReLU(inplace=True)
        scale = 2

        # ENCODER
        # Layer 1
        in_features_1 = in_channels
        out_features_1 = init_features
        self.conv1_1 = nn.Conv3d(in_channels=in_features_1, out_channels=out_features_1, kernel_size=3, padding=1, bias=False,)
        self.norm1_1 = nn.BatchNorm3d(num_features=out_features_1)
        self.conv2_1 = nn.Conv3d(in_channels=out_features_1, out_channels=out_features_1, kernel_size=3, padding=1, bias=False,)
        self.norm2_1 = nn.BatchNorm3d(num_features=out_features_1)
        self.pool_1 = nn.MaxPool3d(kernel_size=2, stride=2)

        # Layer 2
        in_features_2 = out_features_1
        out_features_2 = out_features_1 * scale
        self.drop1_2 = nn.Dropout(p=0.5)
        self.conv1_2 = nn.Conv3d(in_channels=in_features_2, out_channels=out_features_2, kernel_size=3, padding=1, bias=False,)
        self.norm1_2 = nn.BatchNorm3d(num_features=out_features_2)
        self.conv2_2 = nn.Conv3d(in_channels=out_features_2, out_channels=out_features_2, kernel_size=3, padding=1, bias=False,)
        self.norm2_2 = nn.BatchNorm3d(num_features=out_features_2)
        self.pool_2 = nn.MaxPool3d(kernel_size=2, stride=2)
        
        # Layer 3
        in_features_3 = out_features_2
        out_features_3 = out_features_2 * scale
        self.drop1_3 = nn.Dropout(p=0.5)
        self.conv1_3 = nn.Conv3d(in_channels=in_features_3, out_channels=out_features_3, kernel_size=3, padding=1, bias=False,)
        self.norm1_3 = nn.BatchNorm3d(num_features=out_features_3)
        self.conv2_3 = nn.Conv3d(in_channels=out_features_3, out_channels=out_features_3, kernel_size=3, padding=1, bias=False,)
        self.norm2_3 = nn.BatchNorm3d(num_features=out_features_3)
        self.pool_3 = nn.MaxPool3d(kernel_size=2, stride=2)

        # Layer 3A
        in_features_3A = out_features_3
        out_features_3A = out_features_3 * scale
        self.drop1_3A = nn.Dropout(p=0.5)
        self.conv1_3A = nn.Conv3d(in_channels=in_features_3A, out_channels=out_features_3A, kernel_size=3, padding=1, bias=False,)
        self.norm1_3A = nn.BatchNorm3d(num_features=out_features_3A)
        self.conv2_3A = nn.Conv3d(in_channels=out_features_3A, out_channels=out_features_3A, kernel_size=3, padding=1, bias=False,)
        self.norm2_3A = nn.BatchNorm3d(num_features=out_features_3A)
        self.pool_3A = nn.MaxPool3d(kernel_size=2, stride=2)

        # Bottleneck
        in_features_4 = out_features_3A
        out_features_4 = out_features_3A * scale
        self.drop1_4 = nn.Dropout(p=0.5)
        self.conv1_4 = nn.Conv3d(in_channels=in_features_4, out_channels=out_features_4, kernel_size=3, padding=1, bias=False,)
        self.norm1_4 = nn.BatchNorm3d(num_features=out_features_4)
        self.conv2_4 = nn.Conv3d(in_channels=out_features_4, out_channels=out_features_4, kernel_size=3, padding=1, bias=False,)
        self.norm2_4 = nn.BatchNorm3d(num_features=out_features_4)

        # DECODER
        # Layer 3A-up
        self.interp_3A = Interpolate(scale_factor=scale)

        in_features_5A = out_features_4 + out_features_3A
        out_features_5A = int(out_features_4 / scale)
        self.drop1_5A = nn.Dropout(p=0.5)
        self.conv1_5A = nn.Conv3d(in_channels=in_features_5A, out_channels=out_features_5A, kernel_size=3, padding=1, bias=False,)
        self.norm1_5A = nn.BatchNorm3d(num_features=out_features_5A)
        self.conv2_5A = nn.Conv3d(in_channels=out_features_5A, out_channels=out_features_5A, kernel_size=3, padding=1, bias=False,)
        self.norm2_5A = nn.BatchNorm3d(num_features=out_features_5A)

        # Layer 3-up
        self.interp_3 = Interpolate(scale_factor=scale)

        in_features_5 = out_features_5A + out_features_3
        out_features_5 = int(out_features_5A / scale)
        self.drop1_5 = nn.Dropout(p=0.5)
        self.conv1_5 = nn.Conv3d(in_channels=in_features_5, out_channels=out_features_5, kernel_size=3, padding=1, bias=False,)
        self.norm1_5 = nn.BatchNorm3d(num_features=out_features_5)
        self.conv2_5 = nn.Conv3d(in_channels=out_features_5, out_channels=out_features_5, kernel_size=3, padding=1, bias=False,)
        self.norm2_5 = nn.BatchNorm3d(num_features=out_features_5)
        
        # Layer 2-up
        self.interp_2 = Interpolate(scale_factor=scale)

        in_features_6 = out_features_5 + out_features_2
        out_features_6 = int(out_features_5 / scale)
        self.drop1_6 = nn.Dropout(p=0.5)
        self.conv1_6 = nn.Conv3d(in_channels=in_features_6, out_channels=out_features_6, kernel_size=3, padding=1, bias=False,)
        self.norm1_6 = nn.BatchNorm3d(num_features=out_features_6)
        self.conv2_6 = nn.Conv3d(in_channels=out_features_6, out_channels=out_features_6, kernel_size=3, padding=1, bias=False,)
        self.norm2_6 = nn.BatchNorm3d(num_features=out_features_6)

        # Layer 1-up
        self.interp_1 = Interpolate(scale_factor=scale)

        in_features_7 = out_features_6 + out_features_1
        out_features_7 = int(out_features_6 / scale)
        self.drop1_7 = nn.Dropout(p=0.5)
        self.conv1_7 = nn.Conv3d(in_channels=in_features_7, out_channels=out_features_7, kernel_size=3, padding=1, bias=False,)
        self.norm1_7 = nn.BatchNorm3d(num_features=out_features_7)
        self.conv2_7 = nn.Conv3d(in_channels=out_features_7, out_channels=out_features_7, kernel_size=3, padding=1, bias=False,)
        self.norm2_7 = nn.BatchNorm3d(num_features=out_features_7)

        self.final_conv = nn.Conv3d(in_channels=out_features_7, out_channels=out_channels, kernel_size=(1,1,1))

    def forward(self, x):
        # Layer 1
        enc1_1 = self.activation(self.norm1_1(self.conv1_1(x)))
        enc2_1 = self.activation(self.norm2_1(self.conv2_1(enc1_1)))
        pool_1  = self.pool_1(enc2_1)

        # Layer 2
        drop_2 = self.drop1_2(pool_1)
        enc1_2 = self.activation(self.norm1_2(self.conv1_2(drop_2)))
        enc2_2 = self.activation(self.norm2_2(self.conv2_2(enc1_2)))
        pool_2  = self.pool_2(enc2_2)
        
        # Layer 3
        drop_3 = self.drop1_2(pool_2)
        enc1_3 = self.activation(self.norm1_3(self.conv1_3(drop_3)))
        enc2_3 = self.activation(self.norm2_3(self.conv2_3(enc1_3)))
        pool_3  = self.pool_3(enc2_3)

        # Layer 3A
        drop_3A = self.drop1_3(pool_3)
        enc1_3A = self.activation(self.norm1_3A(self.conv1_3A(drop_3A)))
        enc2_3A = self.activation(self.norm2_3A(self.conv2_3A(enc1_3A)))
        pool_3A  = self.pool_3(enc2_3A)

        # Layer 4
        drop_4 = self.drop1_4(pool_3A)
        bottleneck1 = self.activation(self.norm1_4(self.conv1_4(drop_4)))
        bottleneck2 = self.activation(self.norm2_4(self.conv2_4(bottleneck1)))

        # Layer 3A-up
        dec3A = self.interp_3A(bottleneck2)
        dec3A = torch.cat((dec3A, enc2_3A), dim=1)
        drop_5A = self.drop1_5A(dec3A)
        dec1_3A = self.activation(self.norm1_5A(self.conv1_5A(drop_5A)))
        dec2_3A = self.activation(self.norm2_5A(self.conv2_5A(dec1_3A)))

        # Layer 3-up
        dec3 = self.interp_3(dec2_3A)
        dec3 = torch.cat((dec3, enc2_3), dim=1)
        drop_5 = self.drop1_5(dec3)
        dec1_3 = self.activation(self.norm1_5(self.conv1_5(drop_5)))
        dec2_3 = self.activation(self.norm2_5(self.conv2_5(dec1_3)))
        
        # Layer 2-up
        dec2 = self.interp_2(dec2_3)
        dec2 = torch.cat((dec2, enc2_2), dim=1)
        drop_6 = self.drop1_6(dec2)
        dec1_2 = self.activation(self.norm1_6(self.conv1_6(drop_6)))
        dec2_2 = self.activation(self.norm2_6(self.conv2_6(dec1_2)))

        # Layer 1-up
        dec1 = self.interp_1(dec2_2)
        dec1 = torch.cat((dec1, enc2_1), dim=1)
        drop_7 = self.drop1_7(dec1)
        dec1_1 = self.activation(self.norm1_7(self.conv1_7(drop_7)))
        dec2_1 = self.activation(self.norm2_7(self.conv2_7(dec1_1)))

        return self.final_conv(dec2_1)  
         

class Interpolate(nn.Module):
    def __init__(self, scale_factor, mode='nearest'):
        super(Interpolate, self).__init__()
        self.interp = nn.functional.interpolate
        self.scale_factor = scale_factor
        #self.size = size
        self.mode = mode

    def forward(self, x):
        x = self.interp(x, scale_factor=self.scale_factor, mode=self.mode)
        return x

def init_weights_kaiming( net ):
    if isinstance( net, nn.Conv3d ):
        torch.nn.init.kaiming_normal_( net.weight.data, nonlinearity='relu' )

        if not net.bias is None:
            net.bias.data = torch.zeros_like(net.bias)