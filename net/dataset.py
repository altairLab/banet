import os
import random

import numpy as np
import torch
import vtk
import json
from vtk.util import numpy_support
from torch.utils.data import Dataset

class BCDataset(Dataset):
    """Dataset for prediction of Attachment Points"""

    in_channels = 4
    out_channels = 1

    def __init__(
        self,
        input_data,
        binary=True,
        grid_size=(64,64,64),
        grid_side_length=0.3,
        subset='all',
        num_data=10,
        start_ind=0,
        validation_perc=0.2,
        seed=42,
        random_sampling=False,
        vox_filename='voxelized.vts',

    ):

        assert subset in ["all", "train", "validation"]

        if not os.path.exists(input_data):
            raise RuntimeError('Trying to read data from a non-existent folder "{}".'.format(input_data))

        self.input_data         = input_data
        self.binary             = binary
        self.grid_size          = grid_size
        self.grid_side_length   = grid_side_length
        self.random_sampling    = random_sampling
        self.isloaded           = False
        self.vox_filename       = vox_filename
        
        self.samples = list(range(start_ind, start_ind+num_data))
        invalid_samples = []

        # Check if all data exist
        print("Checking if data exist...")
        for i in self.samples:
            filename = os.path.join( input_data, "{:05d}".format(i), vox_filename )
            if not os.path.isfile(filename):
                invalid_samples.append(i)

        # Removing invalid samples
        self.samples = sorted(list(set(self.samples).difference(invalid_samples)))
        print("Total number of VALID samples: {}.".format(len(self.samples)))

        # Select indices of samples in the subset        
        num_data = len(self.samples) #only valid samples
        validation_cases = int(validation_perc*num_data)
        if not subset == "all":
            random.seed(seed)
            validation_samples = random.sample(self.samples, k=validation_cases)
            if subset == "validation":
                self.samples = sorted(list(validation_samples))
                print("Number of validation samples: {}.".format(len(self.samples)))
            else:
                self.samples = sorted(list(set(self.samples).difference(validation_samples))) 
                print("Number of training samples: {}.".format(len(self.samples)))

        self.number_of_samples = len(self.samples)   

    def __len__(self):
        return self.number_of_samples

    def __getitem__(self, idx):
        num_sample = idx
        num_file = self.samples[num_sample]

        if self.random_sampling:
            num_sample = np.random.randint(self.number_of_samples)
            num_file = self.samples[num_sample]

        if self.isloaded:
            x_sample, y_sample = self.data[idx]
            mask = self.mask[idx]
        else:
            filename = os.path.join( self.input_data, "{:05d}".format(num_file), self.vox_filename )
            x_sample, y_sample, mask = self.load_sample(filename)

        input_tensor = torch.from_numpy(x_sample.astype(np.float32))
        output_tensor = torch.from_numpy(y_sample.astype(np.float32))
        mask_tensor = torch.from_numpy(mask.astype(np.bool))

        return input_tensor, output_tensor, mask_tensor

    def load_sample(self, filename):
        gridSize        = self.grid_size
        gridSideLength  = self.grid_side_length

        if not( gridSize[0] == gridSize[1] == gridSize[2]): 
            raise IOError("Grid has different x, y, z dimensions. This case is not handled yet.")
        gridSize = gridSize[0]

        ##################################
        # Read sample
        reader = vtk.vtkXMLStructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        
        voxelized = reader.GetOutput()
        data = voxelized.GetPointData()

        preoperativeRaw = numpy_support.vtk_to_numpy( data.GetArray("preoperativeSurface") )
        preoperative_sdf = np.reshape( preoperativeRaw, (gridSize, gridSize, gridSize, 1) )
        preoperative_sdf = np.transpose(preoperative_sdf, (3,0,1,2) )
        
        max_mask_value = (gridSideLength/gridSize)*np.sqrt(3)
        mask = (preoperative_sdf <= max_mask_value)
        if not mask.any():
            raise IOError("Sample {} contains no internal points (no valid signed distance function?)".format(filename))
        preoperative_sdf = preoperative_sdf*mask

        displacement = None
        if not data.HasArray("displacement"):
            raise IOError("Sample {} does not contain the displacement array".format(filename))
        displacementRaw = numpy_support.vtk_to_numpy( data.GetArray("displacement") )
        displacement = np.reshape( displacementRaw, (gridSize, gridSize, gridSize, 3) )
        displacement = np.transpose( displacement, (3,0,1,2) )
        displacement = displacement*mask

        stiffness = None
        if not data.HasArray("stiffness"):
            raise IOError("Sample {} does not contain the stiffness array".format(filename))
        stiffnessRaw = numpy_support.vtk_to_numpy( data.GetArray("stiffness") )
        if self.binary:
            stiffness_mask = stiffnessRaw > 0
            stiffnessRaw[stiffness_mask] = 1.0
        stiffness = np.reshape( stiffnessRaw, (gridSize, gridSize, gridSize, 1) )
        stiffness = np.transpose(stiffness, (3,0,1,2) )
        
        x_sample = np.append(preoperative_sdf, displacement, axis=0)
        y_sample = stiffness

        return x_sample, y_sample, mask
