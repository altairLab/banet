import torch
from torch import nn
from scipy.spatial import distance
import numpy as np

class DiceCoefficient:
    """ 
    Standard DICE coefficient 
    """
    def __init__(self, smooth=1.0, epsilon=1e-06, **kwargs):
        self.smooth = 1.0
        self.epsilon = epsilon

    def __call__(self, y_pred, y_true, mask=None):
        assert y_pred.size() == y_true.size()

        if mask is not None:
            y_pred = y_pred*mask
        if not len(y_pred.size()) == 1:
            y_pred = y_pred[:, 0].contiguous().view(-1)
            y_true = y_true[:, 0].contiguous().view(-1)

        intersection = (y_pred * y_true).sum() + self.smooth
        denominator = (y_pred.sum() + y_true.sum() + self.smooth).clamp(min=self.epsilon)
        
        dsc = 2. * intersection / denominator
        dsc = dsc.cpu().detach().numpy()
        return float(dsc)

class TPR:
    """
    Computes True Positive Rate, also called Recall or Sensitivity, defined as:
    TP / ( TP + FN ) 
    """

    def __call__(self, y_pred, y_true, mask=None):
        assert y_pred.size() == y_true.size()

        if mask is not None:
            y_pred = y_pred*mask
        if not len(y_pred.size()) == 1:
            y_pred = y_pred[:, 0].contiguous().view(-1)
            y_true = y_true[:, 0].contiguous().view(-1)

        # In case the prediction tensor has not been rounded btw 0 and 1 yet:
        nonzero = y_pred[y_pred > 0].flatten()
        if len( nonzero ) > 0 and not (torch.min( nonzero ) == 1.):
            # print(torch.min( y_pred[y_pred > 0] ) )
            y_pred = torch.round( y_pred )

        intersection = (y_pred * y_true).sum().item() 
        tpr = intersection / len( torch.nonzero(y_true) )
        
        return float(tpr)