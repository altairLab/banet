import torch
from torch import nn

class DiceLoss(nn.Module):
    """ 
    Code borrowed from kaggle segmentation dataset 
    """
    def __init__(self, sigmoid_required=False):
        super(DiceLoss, self).__init__()
        self.smooth = 1.0
        
        self.normalization = None
        if sigmoid_required:
            self.normalization = nn.Sigmoid()

    def forward(self, y_pred, y_true, mask=None):
        assert y_pred.size() == y_true.size()
        if self.normalization:
            y_pred = self.normalization(y_pred)
        if mask is not None:
            y_pred = y_pred*mask
        y_pred = y_pred[:, 0].contiguous().view(-1)
        y_true = y_true[:, 0].contiguous().view(-1)
        intersection = (y_pred * y_true).sum()
        dsc = (2. * intersection + self.smooth) / (
            y_pred.sum() + y_true.sum() + self.smooth
        )
        return 1. - dsc

class BCELogits_Dice_Loss(nn.Module):
    
    def __init__(self, sigmoid_required=True):
        super(BCELogits_Dice_Loss, self).__init__()

        self.bce_logits = nn.BCEWithLogitsLoss()
        self.dsc = DiceLoss(sigmoid_required=sigmoid_required)

    def forward(self, y_pred, y_true, mask=None):

        # Mask should be applied after normalization. This is true for dice,
        # but not for bce with logits, where the mask is applied before normalization
        
        self.cross_entropy = self.bce_logits(y_pred,y_true)
        self.dice = self.dsc(y_pred, y_true, mask=mask)

        return self.cross_entropy+self.dice