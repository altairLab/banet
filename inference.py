import argparse
import os, sys

import numpy as np
import torch

from torch.utils.data import DataLoader
from tqdm import tqdm
import time

sys.path.append('net/')
from dataset import BCDataset as Dataset
from banet import BANet
from loss import BCELogits_Dice_Loss
from metrics import DiceCoefficient, TPR

sys.path.append('utils/')
from utils import Config, filepath
import vtkutils

###############################################################################
######## INFERENCE PARAMETERS
###############################################################################
parser = argparse.ArgumentParser(description="Inference for prediction of attachment points")
parser.add_argument("--config", type=filepath, default="./config/input_parameters.yml", help="Path to config yaml (default: %(default)s).")

# Load configuration file
args = parser.parse_args()
Config.load( args.config )

# Load data
tags          = Config["experiment_tags"]
inputDataDir  = Config["filenames"]["input_datadir"]
predDataDir   = Config["filenames"]["output_datadir"]
voxFilename   = Config["filenames"]["voxelized_filename"]
predFilename  = Config["filenames"]["prediction_filename"]
weights 	  = Config["inference"]["weights"]
start_ind 	  = Config["inference"]["start_ind"]
grid_size 	  = Config["voxelization"]["grid_size"]
grid_side_length = Config["voxelization"]["grid_side_length"]

# Main
def main():

	# Select the experiments of interest
	all_experiments = [f.name for f in os.scandir(inputDataDir) if f.is_dir()]
	experiments     = [f for f in all_experiments for t in tags if t in f]
	experiments 	= sorted(experiments, key=lambda y: int(y[-1]))

	#*** For each boundary & starting configuration
	for exp in experiments:

		experiment_id = [f.name for f in os.scandir( os.path.join(inputDataDir, exp) ) if (f.is_dir() and f.name.isnumeric())]
		experiment_id = sorted(experiment_id, key=lambda y: int(y))

		#*** For each grasping point
		for folder in experiment_id:

			# Number of acquisitions in current folder
			num_data = len( os.listdir( os.path.join(inputDataDir, exp, folder)) ) - start_ind
			
			if num_data > 0:
				curInputDir = os.path.join( inputDataDir, exp, folder )
				curPredDir  = os.path.join( predDataDir, exp, folder )
			
				print("******************************************************")
				print("Running inference for experiment {}, grasping point {}".format(exp, folder))
				run_inference( curInputDir, curPredDir )

def run_inference( input_dir, output_dir ):
	device = torch.device("cpu" if not torch.cuda.is_available() else "cuda:0")
	outfolder = output_dir 
	os.makedirs(outfolder,exist_ok=True) 

	# Create dataset
	dataset = Dataset(input_data=input_dir, subset='all', grid_size=grid_size, grid_side_length=grid_side_length,						start_ind=start_ind, vox_filename=voxFilename)
	loader = DataLoader(dataset, batch_size=1, drop_last=False, num_workers=0)

	# Create the grid
	if not( grid_size[0] == grid_size[1] == grid_size[2]): 
		raise IOError("Grid has different x, y, z dimensions. This case is not handled yet.")
	grid = vtkutils.createGrid(grid_side_length, grid_size[0])

	with torch.set_grad_enabled(False):
		banet = BANet(in_channels=Dataset.in_channels, out_channels=Dataset.out_channels, init_features=8)
		state_dict = torch.load(weights, map_location=device)
		banet.load_state_dict(state_dict)
		banet.eval()
		banet.to(device)

		dsc_bce_loss = BCELogits_Dice_Loss() 

		# Evaluation metrics
		dice = DiceCoefficient()
		tpr = TPR()
	
		true_list = []
		bce_loss_total = []
		pred_time = []
		pred_fname = []

		dice_test   = []
		tpr_test = []

		for i, data in tqdm(enumerate(loader)):
			
			# Starting time measure here, we are including time for data upload to GPU
			torch.cuda.synchronize()  
			t0 = time.time()
			
			x, y_true, mask = data
			x, y_true, mask = x.to(device), y_true.to(device), mask.to(device)

			y = banet(x)
			y_pred = torch.sigmoid(y)*mask
			
			torch.cuda.synchronize()  # Wait for the events to be recorded before measuring time
			pred_time.append(time.time()-t0)
			
			# Loss is computed on y to avoid applying sigmoid twice, since sigmoid is applied inside dsc_bce_loss
			loss = dsc_bce_loss(y, y_true, mask=mask)
			bce_loss_total.append(loss.item())

			# y_pred already has the mask
			dice_test.append(dice(y_pred, y_true))
			tpr_test.append(tpr(y_pred, y_true))
			
			y_pred_np = y_pred.detach().cpu().numpy()
			
			# Create binary output (1 = pred > thresh)
			y_pred_np = np.round(y_pred_np).astype(int)

			output_grid = vtkutils.addDataToGrid(y_pred_np, grid, "stiffness")

			sample_folder = os.path.join( outfolder, "{:05d}".format(loader.dataset.samples[i]))
			output_filename = os.path.join( sample_folder, predFilename )
			if os.path.exists( output_filename ):
				print(f"Removing previous sample {loader.dataset.samples[i]}")
				os.remove( output_filename )
			else:
				os.makedirs(sample_folder,exist_ok=True)
			vtkutils.writeMesh(output_grid, output_filename)

			del y_pred, y_pred_np, output_grid
			
			print(f"Sample ID: {loader.dataset.samples[i]}")
			print(f"Loss on sample: {loss:.6f}")

	bce_avg = np.mean(bce_loss_total)
	bce_std = np.std(bce_loss_total)
	bce_max = np.amax(bce_loss_total)

	print(f"Final avg loss on test dataset: {bce_avg:.8f} \nStd dev: {bce_std:.8f} \nMax error: {bce_max:.8f}")
	print(f"Final avg DICE on test dataset: {np.mean(dice_test):.8f}")
	print(f'Final avg intersection on test dataset: {np.mean(tpr_test)*100:.1f}')

	pred_time_avg = np.mean(pred_time)
	print(f"Average time required for prediction: {pred_time_avg:.8f}")

	loss_filename = os.path.join( outfolder, 'loss' )
	bce_loss_total = np.asarray(bce_loss_total)
	dice_test      = np.asarray(dice_test)
	tpr_test    = np.asarray(tpr_test)
	np.savez_compressed(loss_filename, loss=bce_loss_total, pred_time=pred_time, dice=dice_test, intersection=tpr_test)

if __name__ == "__main__":
	main()
