# Script to voxelize data based on a real point cloud acquired from a vision sensor. 
# Uses ZoomOut to compute corresponding points between the point cloud and a template.
# It is assumed that the point cloud is cleaned, segmented and rigidly aligned with the template.

import sys, os
import argparse
import numpy as np
from plyfile import PlyData
import scipy.sparse.linalg as sla
import robust_laplacian
from pyflann import *
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D

# Figure parameters
plt.rcParams.update({'font.size':18, 'legend.fontsize':18, 'font.family':'serif'})
plt.rcParams.update({'figure.figsize':(16,8)})

import vtk
from vtk.util import numpy_support
sys.path.append('utils/')
from utils import Config, filepath
import vtkutils

parser = argparse.ArgumentParser(description="Voxelizes acquired point clouds to generate network input.")
parser.add_argument("--config", type=filepath, default="./config/input_parameters.yml", help="Path to config yaml (default: %(default)s).")

args = parser.parse_args()
Config.load( args.config )

# Load data
tags          = Config["experiment_tags"]
inputDataDir  = Config["filenames"]["input_datadir"]
outDataDir    = Config["filenames"]["output_datadir"]
surfaceName   = Config["filenames"]["surface_mesh"]
volumeName    = Config["filenames"]["volume_mesh"]
templateName  = Config["filenames"]["template_mesh"]
voxFilename   = Config["filenames"]["voxelized_filename"]
pcdFilename   = Config["filenames"]["pointcloud_filename"]
idxFilename   = Config["filenames"]["indices_filename"]

# Zoomout
N_eigs_init = 10
N_eigs_final = 100
step = 1
flann = FLANN()

# Voxelization
min_visible_displacement = Config["voxelization"]["min_visible_displacement"]
zoomout 		 	     = Config["voxelization"]["zoomout"]
grid_size 	  			 = Config["voxelization"]["grid_size"][0]
grid_side_length	     = Config["voxelization"]["grid_side_length"]
save_figure 		     = Config["voxelization"]["save_figure"]

# Main
def main():

	# Select the experiments of interest
	all_experiments = [f.name for f in os.scandir(inputDataDir) if f.is_dir()]
	experiments     = [f for f in all_experiments for t in tags if t in f]
	experiments 	= sorted(experiments, key=lambda y: int(y[-1]))

	#*** For each boundary & starting configuration
	for exp in experiments:
		# Surface mesh
		surfaceMesh = vtkutils.loadMesh( os.path.join( inputDataDir, exp, surfaceName ) )
		tf = vtkutils.get_centering_transform(surfaceMesh)
		surfaceMesh = vtkutils.apply_vtk_transform( surfaceMesh, tf )

		# voxelize for current exp
		grid = voxelize_surface(surfaceMesh, tf)

		template_filename = os.path.join( inputDataDir, exp, templateName )
		evecs_T, template_vertices = get_template_evecs(template_filename)
		templateSurface = vtkutils.loadMesh( template_filename )
		templateSurface = vtkutils.apply_vtk_transform( templateSurface, tf )

		experiment_id = [f.name for f in os.scandir( os.path.join(inputDataDir, exp) ) if (f.is_dir() and f.name.isnumeric())]
		experiment_id = sorted(experiment_id, key=lambda y: int(y))

		#*** For each grasping point
		for folder in experiment_id:

			print("******************************************************")
			print("Running voxelization for experiment {}, grasping point {}".format(exp, folder))

			# Names of the pcds at various deformations
			folder_names = [f.name for f in os.scandir( os.path.join(inputDataDir, exp, folder)) if f.is_dir()]
			folder_names = sorted(folder_names, key=lambda x: int(x))

			#*** For each level of lifting
			for name in folder_names:
			
				curDir = os.path.join(inputDataDir, exp, folder, name )
				outDir = os.path.join(outDataDir, exp, folder, name )

				# Load visible points, convert and transform them
				intraopSurface = vtkutils.loadMesh( os.path.join( curDir, pcdFilename ) )
				vtkutils.addVertsToPolyData( intraopSurface )
				intraopSurface = vtkutils.apply_vtk_transform( intraopSurface, tf )

				if not zoomout:
					# Load existing correspondences
					closestIndices = np.loadtxt( os.path.join( curDir, 'idxs.txt'), dtype=int )
					# Compute displacement and interpolate to grid
					output_grid = voxelize_displacement(grid, intraopSurface, surfaceMesh, closestIndices)
				else:						
					# Use zoomout to compute correspondences
					closestIndices = get_corresponding_points(intraopSurface, template_vertices, evecs_T, outDir, save=save_figure)
					# Compute displacement and interpolate to grid
					output_grid = voxelize_displacement(grid, intraopSurface, templateSurface, closestIndices)

				# Save voxelized grid
				filename = os.path.join( outDir, voxFilename )
				print("Writing to {}".format( filename ))
				vtkutils.writeMesh( output_grid, filename )

def voxelize_surface(surface, tf):
	# Calculate the signed distance field for the preoperative surface, store it in grid
	print("Voxelizing preoperative (undeformed) state")
	grid = vtkutils.createGrid( grid_side_length, grid_size)
	vtkutils.distanceField( surface, grid, "preoperativeSurface", signed=True )

	# Write the applied transform into a field data array
	vtkutils.storeTransformationMatrix( grid, tf )
	return grid

def get_template_evecs(template_name):
	template = PlyData.read(template_name)
	template_vertices = np.vstack((
		template['vertex']['x'],
		template['vertex']['y'],
		template['vertex']['z']
	)).T.astype(np.float32)
	template_faces = np.vstack(template['face']['vertex_indices'])

	L_T, M_T = robust_laplacian.mesh_laplacian(template_vertices, template_faces)
	n_eig = N_eigs_final
	__, evecs_T = sla.eigsh(L_T, n_eig, M_T, sigma=1e-8)

	return evecs_T, template_vertices

def get_corresponding_points(pcd, template_vertices, evecs_T, directory, save=False):
	intraop_np = numpy_support.vtk_to_numpy( pcd.GetPoints().GetData() )

	print("Computing corresponding points with ZoomOut...")
	# Laplacian on pcl
	L, M = robust_laplacian.point_cloud_laplacian(intraop_np)
	__, evecs_pc = sla.eigsh(L, N_eigs_final, M, sigma=1e-8)
	
	# kNN to get first indices
	result_ini, dists = flann.nn(template_vertices, intraop_np.astype(np.float32))
	
	# Applying ZOOM-OUT
	C_iter = np.matmul(np.linalg.pinv(evecs_pc[:,0:N_eigs_init]),evecs_T[result_ini,0:N_eigs_init])
	for i in np.arange(0,N_eigs_final - N_eigs_init, step):
		# 1) Convert into dense correspondence
		evecs_transf = np.matmul(evecs_T[:,0:N_eigs_init+i], C_iter.T)
		result, dists = flann.nn(evecs_transf, evecs_pc[:,0:N_eigs_init+i], 1)
		# 2) Convert into C of dimension (n+1) x (n+1)
		C_iter = np.matmul(np.linalg.pinv(evecs_pc[:,0:N_eigs_init+i+1]),evecs_T[result,0:N_eigs_init+i+1])
	
	np.savetxt( os.path.join( directory, idxFilename ), result, fmt='%d' )

	if save:
		# One color per point in template
		c = np.abs(np.cos( template_vertices*40 ))

		fig = plt.figure()
		ax1 = fig.add_subplot(1, 2, 1, projection='3d')

		ax1.scatter3D(template_vertices[:,0], template_vertices[:,1], template_vertices[:,2], c=c, s=5)
		ax1.set_xlim(-0.1,0.1)
		ax1.set_ylim(-0.1,0.1)
		ax1.set_zlim(-0.1,0.1)

		ax1.set_xlabel('X')
		ax1.set_ylabel('Y')
		ax1.set_zlabel('Z')
		ax1.view_init(elev=0, azim=90)
		#print(ax1.azim, ax1.elev)
		ax1.set_title('Template')

		c_corresp 		 = c[result]
		ax2 = fig.add_subplot(1, 2, 2, projection='3d')
		ax2.scatter3D(intraop_np[:,0], intraop_np[:,1], intraop_np[:,2], c=c_corresp, s=15)
		ax2.set_xlim(-0.1,0.1)
		ax2.set_ylim(-0.1,0.1)
		ax2.set_zlim(-0.1,0.1)
		ax2.set_xlabel('X')
		ax2.set_ylabel('Y')
		ax2.set_zlabel('Z')
		ax2.view_init(elev=0, azim=90)
		ax2.set_title('Point cloud')

		fig_name = os.path.join( directory, f"{idxFilename[:-4]}.png")
		plt.savefig(fig_name)

	return result

def voxelize_displacement(grid, pcd, mesh, idx):
	displacement = vtkutils.compute_warp_displacement( mesh, pcd, idx, min_visible_displacement=min_visible_displacement )
	mesh.GetPointData().AddArray( displacement )

	cellSize = grid_side_length/grid_size
	output_grid = vtkutils.interpolateToGrid( mesh, grid, cellSize )
	return output_grid

if __name__ == "__main__":
	main()